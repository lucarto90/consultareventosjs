// instaniar ambas clases
const eventBrite = new EventBrite();
const ui = new Interfaz();

// listener al buscador
document.getElementById('buscarBtn').addEventListener('click', (e) => {
    e.preventDefault();

    // leer el texto del input buscar
    const textoBuscador = document.getElementById('evento').value;

    // leer el select
    const categorias = document.getElementById('listado-categorias');
    const categoriaSeleccionada = categorias.options[categorias.selectedIndex].value;

    // revisar que haya algo escrito en el buscador
    if (textoBuscador !== '') {
        // cuando si hay una busqueda
        eventBrite.obtenerEventos(textoBuscador, categoriaSeleccionada)
            .then(eventos => {
                // if (eventos.eventos.events.length > 0 ) {
                if (eventos.eventos.status_code !== 404 ) {
                    // si hay eventos mostrar el resultado
                    ui.limpiarResultados();
                    ui.mostrarEventos(eventos.eventos);
                } else {
                    // no hay eventos enviar una alerta
                    ui.mostrarMensaje('No hay resultados', 'alert alert-danger mt-4');
                }
            })
        ;
    } else {
        // mostrar mensaje para que imprima algo
        ui.mostrarMensaje('Escribe lo que deseas buscar', 'alert alert-danger mt-4');
    }
});